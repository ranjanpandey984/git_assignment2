const players = ["John", "Tenz", "Zombs", "Shroud", "Ninja"];

// Returns the first element that satisfies the condition
const foundPlayer = players.find((players) => players.length > 3);

// Returns all the elements that satisfies the condition
const filterPlayer = players.filter((players) => players.length > 4);

console.log(foundPlayer);
console.log(filterPlayer);

// Creates a new array by applying the provided condition to each element of the array
const numbers = [0, 2, 4, 6, 9, 12, 25, 36];
const newNumbers = numbers.map((num) => num * 2);
console.log(newNumbers);

const squareRoots = numbers.map((num) => Math.sqrt(num));
console.log(squareRoots);

// Returns an array having the elements between two specified indexes.
const sliceNumber1 = numbers.slice(2); // Elements from 2nd index to last index
console.log(sliceNumber1);

const sliceNumber2 = numbers.slice(3, 6); // Elements from 4th element to 6th element excluding 6th element
console.log(sliceNumber2);

const sliceNumber3 = numbers.slice(-3); // Last three elements
console.log(sliceNumber3);
