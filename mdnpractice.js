let foods = ["Pizza", "Momo", "Burger", "Noodles"];
console.log(foods);

console.log(foods.pop()); // Removes and returns the last element of an array
console.log(foods);

foods.push("Icecream", "Rolls"); // Adds the elements after the last element of an array
console.log(foods);

foods.unshift("Roti", "Sandwich"); // Adds the elements before the first element of an array
console.log(foods);

console.log(foods.shift()); // Removes and returns the first element of an array
console.log(foods);

let sports = ["Cricktet", "Basketball", "Football"];
let players = ["Sachin Tendulkar", "Michael Jordan", "Lionel Messi"];

sports.push(players); // Adds the whole array at last as one element in another array
console.log(sports);

sports.unshift(...players); // Adds the elements of an array at the front of another array
console.log(sports);
